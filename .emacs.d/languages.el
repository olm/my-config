;; https://github.com/ejmr/php-mode
(require 'php-mode)

;; http://web-mode.org/
;(require 'web-mode)

;; GNU Octave
(add-to-list 'auto-mode-alist '("\\.m\\'" . octave-mode))

;; PostgreSQL
(add-to-list 'auto-mode-alist
             '("\\.psql$" . (lambda ()
                              (sql-mode)
                              (sql-highlight-postgres-keywords))))

;; XML Schema
(add-to-list 'auto-mode-alist '("\\.xsd$" . xml-mode))

;; XQuery mode
(require 'xquery-mode)
(autoload 'xquery-mode "xquery-mode" "XQuery mode" t)
(add-to-list 'auto-mode-alist '("\\.xqy$"    . xquery-mode))
(add-to-list 'auto-mode-alist '("\\.xquery$" . xquery-mode))

;; http://jblevins.org/projects/markdown-mode/
(autoload 'markdown-mode "markdown-mode" "Major mode for editing Markdown files" t)
(add-to-list 'auto-mode-alist '("\\.md$'"       . markdown-mode))
(add-to-list 'auto-mode-alist '("\\.mkd$'"      . markdown-mode))
(add-to-list 'auto-mode-alist '("\\.markdown$'" . markdown-mode))

(require 'tex)
(require 'latex)
(TeX-global-PDF-mode t)
(require 'magic-latex-buffer) ; https://github.com/zk-phi/magic-latex-buffer

;; latexmk https://github.com/uwabami/auctex-latexmk
;(require 'auctex-latexmk)
;(auctex-latexmk-setup)

(require 'rainbow-mode)
;; CSS and rainbow modes
(defun all-css-modes() (css-mode) (rainbow-mode))
;; Load both major and minor modes in one call based on file type
(add-to-list 'auto-mode-alist '("\\.css$" . all-css-modes))

(provide 'languages.el)
;;; languages.el ends here
