;; https://www.gnu.org/software/emacs/manual/html_node/emacs/Custom-Themes.html
(if (display-graphic-p)
    (custom-set-variables
     '(custom-enabled-themes (quote (deeper-blue))))
)

;; ToolBar
(tool-bar-mode -1)

;; MenuBar
;(menu-bar-mode -1)

;; https://www.gnu.org/software/emacs/manual/html_node/efaq/Displaying-the-current-line-or-column.html
(setq line-number-mode   t)
(setq column-number-mode t)

;; https://www.gnu.org/software/emacs/manual/html_node/emacs/Uniquify.html
(require 'uniquify)
(setq uniquify-buffer-name-style 'post-forward-angle-brackets)

;; Auto-complete https://github.com/auto-complete/auto-complete
;; For Debian https://packages.debian.org/stable/auto-complete-el
(require 'auto-complete-config)
(ac-config-default)
; https://github.com/monsanto/auto-complete-auctex
(require 'auto-complete-auctex)

(provide 'ui.el)
;;; ui.el ends here
